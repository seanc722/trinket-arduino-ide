
 
int latchPin = 3;
int clockPin = 4;
int dataPin = 0;


int dataRowsG;
int dataColsG;
int dataRowsR;
int dataColsR;

int curCol = 0;
byte cols[5];

void setup() 
{
  pinMode(latchPin, OUTPUT);
  pinMode(dataPin, OUTPUT);  
  pinMode(clockPin, OUTPUT);
  
  clearRegisters();
}





void clearRegisters() {
  cols[0] = 0xFF;
  cols[1] = 0xFF;
  cols[2] = 0xFF;
  cols[3] = 0xFF;
  cols[4] = 0xFF;
  //updateShiftRegister();
}


int r = 1;
void loop() 
{
  
  /*if (r) {
    if (dataRowsG < 128) {
      dataRowsG = dataRowsG * 2;
    } else { 
      dataRowsG = 0x01;
      if (dataColsG < 16) {
        dataColsG = dataColsG * 2;
      } else {
        dataColsG = 0;
        dataRowsG = 0;
        dataColsR = 1;
        dataRowsR = 1;
        r=0; 
      }

    }
  } else {
    if (dataRowsR < 128) {
      dataRowsR = dataRowsR * 2;
    } else { 
      dataRowsR = 0x01;
      if (dataColsR < 16) {
        dataColsR = dataColsR * 2;
      } else {
        dataColsR = 0;
        dataRowsR = 0;
        dataColsG = 1;
        dataRowsG = 1;
        r = 1; 
      }
    }
  }*/
 
 
 // registers[1] = 1; 
//  registers[0] = 1;
  //registers[1] = 1;
 // registers[10] = 1;
 
//  updateShiftRegister();
 // dataRowsR = dataRowsR >> 1;
 // dataRowsG = dataRowsG >> 1;
  
  for(int i=0; i < 5; i++) {
    int c;
    switch(i) {
     case 0: c = 1; break;
    case 1: c= 2; break;
   case 2: c = 4; break;
  case 3: c= 8; break;
 case 4: c = 16; break; 
    }
   updateShiftRegister(c);
   delay(500);
  }
}


void updateShiftRegister(byte col)
{
   digitalWrite(latchPin, LOW);
 
   //for(int i = numPins; i >= 0; i--) {
     digitalWrite(clockPin, LOW);
   
     
   
 /*    redCols = dataColsR & 0xff;
     greenCols = dataColsG  & 0xff;
     redRows = dataRowsR & 0xff;
     greenRows = dataRowsG & 0xff;
   */  
     byte bcol = int(pow(2,col)) & 0xff;
     byte d = cols[col] & 0xff;
     
     shiftOut(dataPin, clockPin, MSBFIRST, 0x00);
     shiftOut(dataPin, clockPin, MSBFIRST, col);
     shiftOut(dataPin, clockPin, MSBFIRST, 0x00);
     shiftOut(dataPin, clockPin, MSBFIRST, 0xff);
    // digitalWrite(dataPin, registers[i]);
    
   //  digitalWrite(clockPin, HIGH); 
  // }
   digitalWrite(latchPin, HIGH);
}
