      /*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.
 
  This example code is in the public domain.
 
  To upload to your Gemma or Trinket:
  1) Select the proper board from the Tools->Board Menu
  2) Select USBtinyISP from the Tools->Programmer
  3) Plug in the Gemma/Trinket, make sure you see the green LED lit
  4) For windows, install the USBtiny drivers
  5) Press the button on the Gemma/Trinket - verify you see
     the red LED pulse. This means it is ready to receive data
  6) Click the upload button above within 10 seconds
*/
 
int row1 = 0; // blink 'digital' pin 1 - AKA the built in red LED
int row2 = 2;

int col1 = 4;
int col2 = 3;

// the setup routine runs once when you press reset:
void setup() {
  // initialize the digital pin as an output.
  pinMode(row1, OUTPUT);
  pinMode(row2, OUTPUT);
  pinMode(col1, OUTPUT);
  pinMode(col2, OUTPUT);
  
  pinMode(1, OUTPUT);
  
}


// the loop routine runs over and over again forever:
void loop() {
  digitalWrite(row1, HIGH);
  for(int x = 0; x < 30000; x++) {
    digitalWrite(col1, HIGH);
    digitalWrite(col1, LOW);
    digitalWrite(col2, HIGH);
    digitalWrite(col2, LOW);
  }
  
  digitalWrite(row1, LOW);

  digitalWrite(row2, HIGH);
  for(int x = 0; x < 30000; x++) {
    digitalWrite(col1, HIGH);
    digitalWrite(col1, LOW);
    digitalWrite(col2, HIGH);
    digitalWrite(col2, LOW);
  }
  digitalWrite(row2, LOW);

}
