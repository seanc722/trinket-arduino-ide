int redPin = 0;
int greenPin = 1;
int bluePin = 4;

// the setup routine runs once when you press reset:
void setup() {
  // initialize the pins as an output.
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
}


int r, g, b;
// the loop routine runs over and over again forever:
void loop() {
  setColor(255, 0, 0);
  delay(1000);
  setColor(0, 255, 0);
  delay(1000);
  setColor(0, 0, 255);
  delay(1000);
  
  r = 255;
  g = 0;
  b = 0;
  while(b < 255) {
    b += 15;
    setColor(r,g,b);
    delay(100); 
  }
  while(r > 0) {
    r -= 15;
    setColor(r,g,b);
    delay(100); 
  }
  while(g < 255) {
    g += 15;
    setColor(r,g,b);
     delay(100); 
  }
  while(b > 0) {
     b -= 15;
     setColor(r,g,b);
     delay(100); 
  }
  while(r < 255) {
     r+= 15;
     setColor(r,g,b);
      delay(100); 
  }
  while(g > 0) {
    g -= 15;
    setColor(r,g,b);
   delay(100); 
  }
}

void setColor(int red, int green, int blue) {
  analogWrite(redPin, 255-red);
  analogWrite(greenPin, 255-green);
  analogWrite(bluePin, 255-blue);
}

