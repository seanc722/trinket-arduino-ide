
 
int latchPin = 3;
int clockPin = 4;
int dataPin = 0;
 
#define numOfPins 8

int registers[numOfPins];

void setup() 
{
  pinMode(latchPin, OUTPUT);
  pinMode(dataPin, OUTPUT);  
  pinMode(clockPin, OUTPUT);
}
 

int curReg = 0; 
int val = 1;
int dir = 1;
void loop() 
{
  registers[curReg += dir] = val;
    
  updateShiftRegister();
  delay(50);
  if (curReg == 7 and dir == 1 && val) {
   curReg = 0;
   val = 0; 
  } else if(curReg == 7 and dir == 1 && !val) {
    val = 1;
    dir = -1; 
  }
  if (curReg == 0 and dir == -1 && val) {
    curReg = 7;
    val = 0; 
  } else if (curReg == 0 and dir == -1 and !val) {
    val = 1;
    dir = 1;
  }
}
 
void updateShiftRegister()
{
   digitalWrite(latchPin, LOW);
 
   for(int i = 0; i < numOfPins; i++) {
     digitalWrite(clockPin, LOW);
     
     digitalWrite(dataPin, registers[i]);
    
     digitalWrite(clockPin, HIGH); 
   }
   digitalWrite(latchPin, HIGH);
}
