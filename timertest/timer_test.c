#define F_CPU 8000000UL

#include <avr/interrupt.h>
#include <avr/io.h>

//Store pin for nice naming
int LEDpin = PB1;
static char toggle = 0;
int counter = 0;


void setup() {
	//COM0A1   Compare match output A mode - CLEAR OCOA on compare match
   TCCR0A = (1 << COM0A1); 
   
   //CS00 -> No prescaling
   //CS01 -> clk/8
   //CS01 & CS00  -> clk/64
   //CS02	-> clk/256
   //CS02 & CS00 -> clk/1024
   TCCR0B = (1 << CS02) | (1 << CS00); 
   
   //COMPARE TO 255
   OCR0A = 0xFF;
   
   //OCIE0A - Output compare match A interrupt
   //TOIE0 - overflow interrupt 
   TIMSK = (1 << OCIE0A);

	//Enable global interrupts
	sei();
	
	//Set pin1 output
	DDRB |= (1 << LEDpin);
}

SIGNAL(TIM0_COMPA_vect)
{
	if (counter == 15) {
		// toggle the LED on each interrupt    
		if (toggle) {
			toggle = 0;
			PORTB &= ~(1 << LEDpin);
		} else {
			toggle = 1;
			PORTB |=  (1 << LEDpin);
		}
	}
	counter = (counter >= 15) ? 0 : counter + 1;
}




int main(void) {
	setup();	
		
	while (1) {

	}	

	return 1;
}

