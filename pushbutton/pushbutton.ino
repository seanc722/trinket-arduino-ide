      /*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.
 
  This example code is in the public domain.
 
  To upload to your Gemma or Trinket:
  1) Select the proper board from the Tools->Board Menu
  2) Select USBtinyISP from the Tools->Programmer
  3) Plug in the Gemma/Trinket, make sure you see the green LED lit
  4) For windows, install the USBtiny drivers
  5) Press the button on the Gemma/Trinket - verify you see
     the red LED pulse. This means it is ready to receive data
  6) Click the upload button above within 10 seconds
*/
 
int led = 4; // blink 'digital' pin 1 - AKA the built in red LED
 
int pushButton = 2;

int power = HIGH;

int lastState = 0;
// the setup routine runs once when you press reset:
void setup() {
  // initialize the digital pin as an output.
  pinMode(led, OUTPUT);
  pinMode(pushButton, INPUT);
  
  digitalWrite(led, power);
}



// the loop routine runs over and over again forever:
void loop() {
  if (digitalRead(pushButton)) {  // if the button is pressed
    if (power == LOW) {
      power = HIGH; 
    } else if (power == HIGH) {
      power = LOW;
    }

    digitalWrite(led, power);
    delay(500);
  }
  
  
}
