#define F_CPU 16000000UL //8mhz clock

#include <avr/interrupt.h>
//#include <avr/io.h>
#include <util/delay.h>

#define LAYERS 5
#define COLS 5
#define ROWS 5
#define EXTRA 32 - (COLS * ROWS + LAYERS)

#define dataPin PB0
#define latchPin PB3
#define clockPin PB4

#define ANIM_DELAY_HEART 500
#define MAX_BRIGHTNESS 4

void digitalWrite(int, int);
void clearLEDs();
void shiftOut();
void push(int);
void setupTimer();


/* Animations */
void animScan();
void animRain();
void animHeart();

int leds[LAYERS][ROWS][COLS];

int curLayer = 0;

int main(void) {
	/* Set latch, data and clock pins as output pins */
	DDRB |= ((1 << latchPin) | (1 << dataPin) | (1 << clockPin) | (1 << PB1));

	/* Write 0 to LED array */
	clearLEDs();
	
	/* Setup the timer */
	setupTimer();
	
	/* Begin main loop */
	while (1) {
		animHeart();
		//animScan();
		//rain();
	}	

	return 1;
}

/* animRain() - Animaion, show falling LED's, sort of like rain drops
*/
void animRain() {
	int x, y;
	int r;
	int spawn;
	
	for(x = 0; x < 50; x++) {
		//How many leds to attempt to generate
		spawn = rand() % 5; 
		
		//Generate LEDs in random position on top layer
		for(y=0; y < spawn; y++) {
			r = rand() % 25;
			*(&leds[LAYERS-1][0][0] + r) = 1;
		}
		_delay_ms(5);
		
		//Shift the layers down
		int *led = &leds[0][0][0];
		int *last = &leds[LAYERS-1][0][0];
		for(; led < last; led++) {
			*led = *(led + 25);
		}
		last = &leds[LAYERS-1][ROWS-1][COLS-1];
		for(; led <= last; led++) *led = 0;
	}
	return;
}


/* animHeart() - Animation, display a heart that rotates around the face of the cube
*/
void animHeart() {
	clearLEDs();
	leds[0][0][2] = 1;
	leds[1][0][1] = 1;
	leds[1][0][3] = 1;
	leds[2][0][0] = 1;
	leds[2][0][4] = 1;
	leds[3][0][0] = 1;
	leds[3][0][2] = 1;
	leds[3][0][4] = 1;
	leds[4][0][1] = 1;
	leds[4][0][3] = 1;
	
	_delay_ms(ANIM_DELAY_HEART);
	int l, x, y, temp;
	for(l = 0; l < LAYERS; l++) {
		for(x = 0; x < ROWS; x++) {
			for(y = x; y < COLS; y++) {
				temp = leds[l][x][y];
				leds[l][x][y] = leds[l][y][x];
				leds[l][y][x] = temp;
			}
		}
	}
	_delay_ms(ANIM_DELAY_HEART);
	
	for(l = 0; l < LAYERS; l++) {
		for(x = 0; x < ROWS; x++) {
			temp = leds[l][x][0];
			leds[l][x][0] = 0;
			leds[l][4][4-x] = temp;
		}
	}
	_delay_ms(ANIM_DELAY_HEART);
	
	for(l = 0; l < LAYERS; l++) {
		for(x = 0; x < ROWS; x++) {
			for(y = x; y < COLS; y++) {
				temp = leds[l][x][y];
				leds[l][x][y] = leds[l][y][x];
				leds[l][y][x] = temp;
			}
		}
	}
	_delay_ms(ANIM_DELAY_HEART);
	
}

/* animScan() - Animation, scan through each of the led's one at a time
*/
void animScan() {
	int *led = &leds[0][0][0];
	int *last = &leds[LAYERS-1][ROWS-1][COLS-1];
	for(; led <= last; led++) {
		*led = 1;
		shiftOut();
		_delay_ms(10);
		*led = 0;
	}
}

/*	digitalWrite() - Sets a given pin to a given value
	@param	int		pin: Pin number to write on
	@param	int		val: Value to write out on the pin
*/
void digitalWrite(int pin, int val) {
	if (val) 
		PORTB |= (1 << pin); //Write out a 1
	else
		PORTB &= ~(1 << pin); //Write out a 0
	return;
}

/* clearLEDs() - Clear the leds array
*/
void clearLEDs() {
	int * last = &leds[LAYERS-1][ROWS-1][COLS-1];
	int * iter = leds[0][0];
	for(; iter <= last; iter++) {
		*iter = 0;
	}
	return;
}

/* shiftOut() - Writes out leds array of the current layer to the registers
*/
void shiftOut() {
	int x, y;
	
	digitalWrite(latchPin, 0);
	
	/* Write out the values of the column and rows */
	for(x = 0; x < ROWS; x++) {
		for(y = 0; y < COLS; y++) {
			push(leds[curLayer][x][y]);
		}
	}
	
	/* Write out the layers, 0 for all except the current layer */
	for(x = 0; x < LAYERS; x++) {
		push(x == curLayer);
	}
	
	/* Write out the extra pins that arn't in use */
	for(x = 0; x < EXTRA; x++) push(0); 
	
	digitalWrite(latchPin, 1);
	
	return;
}

/*	push() - Pushes out given value on the data pin (HIGH or LOW)
	@param	int		val: Value to set on data pin (1-High, 0-Low)
*/
void push(int val) {
	digitalWrite(clockPin, 0);
	digitalWrite(dataPin, val);
	digitalWrite(clockPin, 1);
}



/*	setupTimer() - sets up the interrupt timer to trigger the LED refresh
*/
void setupTimer() {
	cli();
	
	/* Clear TCCR1 register before configuring */
	TCCR1 = 0;
	 
	/* Set to clear timer on compare match */
	TCCR1 |= (1 << CTC1);
	
	/* Set clock to Async mode */
	PLLCSR |= (1 << PCKE);
	
	/* Set /1024 prescaler (CS13, CS11, CS10) */
	TCCR1 |= (1 << CS10);
    TCCR1 |= (1 << CS11);
	//TCCR1 |= (1 << CS12);
	TCCR1 |= (1 << CS13);
	 
	/* Zero (Reset) Timer1 counter value */
	TCNT1 = 0;

	/* enable Timer1 COMPA compare interrupt */
	TIMSK |= (1 << OCIE1A);
	
	/* Set compare to 52.  
	   16mhz / 1024(prescaler) = 15,625
	   x = 15,625 / 300hz = ~ 52. 
	   My thought process and math may be way off but it works :)
	*/
	OCR1A = 52; 
	
	/* Enable global interrupts */
	sei();
}

/* Triggered on compare match */
ISR(TIM1_COMPA_vect) {
	shiftOut();	
	
	if (curLayer + 1 < LAYERS) {
		curLayer++;
	} else {
		curLayer = 0;
	}
}