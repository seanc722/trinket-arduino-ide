      /*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.
 
  This example code is in the public domain.
 
  To upload to your Gemma or Trinket:
  1) Select the proper board from the Tools->Board Menu
  2) Select USBtinyISP from the Tools->Programmer
  3) Plug in the Gemma/Trinket, make sure you see the green LED lit
  4) For windows, install the USBtiny drivers
  5) Press the button on the Gemma/Trinket - verify you see
     the red LED pulse. This means it is ready to receive data
  6) Click the upload button above within 10 seconds
*/
 
// the setup routine runs once when you press reset:
void setup() {
    
  resetPins();
}

const int total_leds = 6; //Hold LED Count

//Hold 2 pin combo for each LED
int leds[total_leds * 2] = {2, 3, 
                            2, 4,
                            4, 3,
                            3, 4,
                            4, 2,
                            3, 2
};


void resetPins() {
     for(int x = 0; x <= 5; x++) {
      pinMode(x, INPUT);
      digitalWrite(x, LOW); 
    } 
}

void led(int num) {
  resetPins();
  int p1 = leds[num*2];
  int p2 = leds[num*2+1];
  
  pinMode(p1, OUTPUT);
  digitalWrite(p1, HIGH); 
  pinMode(p2, OUTPUT);
  digitalWrite(p2, LOW);
  
}

int led_states[6] = {1, 0, 0, 0, 0, 0};
int current_led = 0;
// the loop routine runs over and over again forever:

long counter = 0;
void loop() {
  for(int x = 0; x < total_leds; x++) {
    if (led_states[x]) led(x); 
  }
  
  if (counter % 2000 == 0) {
    int r = random(0, total_leds);
    led_states[r] = !led_states[r];
    counter=0;
  }
  counter++;
 
}
