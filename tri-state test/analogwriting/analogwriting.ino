      /*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.
 
  This example code is in the public domain.
 
  To upload to your Gemma or Trinket:
  1) Select the proper board from the Tools->Board Menu
  2) Select USBtinyISP from the Tools->Programmer
  3) Plug in the Gemma/Trinket, make sure you see the green LED lit
  4) For windows, install the USBtiny drivers
  5) Press the button on the Gemma/Trinket - verify you see
     the red LED pulse. This means it is ready to receive data
  6) Click the upload button above within 10 seconds
*/
 
// the setup routine runs once when you press reset:
    int p0 = 0;
    int p1 = 1;
void setup() {

    
    pinMode(p0, OUTPUT);
    pinMode(p1, OUTPUT);

}

int dir = 1;
int dutyc = 0;
void loop() {
  
  analogWrite(p0, dutyc);
  analogWrite(p1, dutyc);
  
  dutyc += dir;
  if (dutyc == 255 || dutyc == 0) {
    dir = -dir; 
  }

  if (dutyc < 75) delay(5);
  delay(5);

   
 
}
