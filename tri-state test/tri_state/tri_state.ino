      /*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.
 
  This example code is in the public domain.
 
  To upload to your Gemma or Trinket:
  1) Select the proper board from the Tools->Board Menu
  2) Select USBtinyISP from the Tools->Programmer
  3) Plug in the Gemma/Trinket, make sure you see the green LED lit
  4) For windows, install the USBtiny drivers
  5) Press the button on the Gemma/Trinket - verify you see
     the red LED pulse. This means it is ready to receive data
  6) Click the upload button above within 10 seconds
*/
 


int pinl = 0;
int pinr = 3;

// the setup routine runs once when you press reset:
void setup() {
    
  resetPins();
}

void resetPins() {
     for(int x = 0; x <= 5; x++) {
      pinMode(x, OUTPUT);
      digitalWrite(x, LOW); 
    } 
}
// the loop routine runs over and over again forever:
void loop() {
  
  pinMode(3, OUTPUT);
  digitalWrite(3, HIGH); 
  delay(500);

  resetPins();
  
  pinMode(2, OUTPUT);
  digitalWrite(2, HIGH);
  
  delay(500);
  resetPins();
   
  
}
