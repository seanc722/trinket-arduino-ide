
 
int latchPin = 3;
int clockPin = 4;
int dataPin = 0;
 
#define numOfPins 8

int registers[numOfPins];

void setup() 
{
  pinMode(latchPin, OUTPUT);
  pinMode(dataPin, OUTPUT);  
  pinMode(clockPin, OUTPUT);
}
 

int curReg = 0; 

int dir = 1;


void loop() 
{
  registers[0] = 0;
  registers[1] = 1;
  registers[2] = 1;
  registers[3] = 1;
  
  registers[4] = 0;
  registers[5] = 0;
  registers[6] = 0;
  registers[7] = 1;
 updateShiftRegister();
 clearRegisters();
 delay(5000);

}
int clearRegisters() {
 for(int i=0; i < 4; i++) {
    registers[i] = 1;
 } 
 for(int i = 0; i < 4; i++) {
   registers[i+4] = 0; 
 }
}
void updateShiftRegister()
{
   digitalWrite(latchPin, LOW);
 
   for(int i = 0; i < numOfPins; i++) {
     digitalWrite(clockPin, LOW);
     
     digitalWrite(dataPin, registers[i]);
    
     digitalWrite(clockPin, HIGH); 
   }
   digitalWrite(latchPin, HIGH);
}
